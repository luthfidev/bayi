package com.example.ghea.stuntingApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.ghea.stuntingApp.utils.SharedPrefManager;

public class Menu extends AppCompatActivity {

    EditText user,pass;
    Button data,cek, keluar;
    ProgressDialog pDialog;
    String u,p,pesan;
    int sukses;
    SharedPrefManager sharedPrefManager;  // after login

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

            setContentView(R.layout.menu);
        sharedPrefManager = new SharedPrefManager(this); // after login
            pDialog = new ProgressDialog(this);
            data = (Button) findViewById(R.id.buttonanak);
            cek = (Button) findViewById(R.id.buttoncek);
            keluar = (Button) findViewById(R.id.buttonlogout);



            data.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(),Identifikasilama.class);
                    startActivity(i);
                }
            });

            cek.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(),Ortu.class);
                    startActivity(i);
                }
            });
        keluar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                startActivity(new Intent(Menu.this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });

        }

}
