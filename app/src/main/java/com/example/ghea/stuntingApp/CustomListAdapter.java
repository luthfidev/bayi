package com.example.ghea.stuntingApp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by febry0putra on 02/01/19.
 */


    public class CustomListAdapter extends ArrayAdapter<String> {

        private final Activity context;
        private final ArrayList<String> arid;
        private final ArrayList<String> arname;
        private final ArrayList<String> aribu;
        private final ArrayList<String> ardata;
        private final ArrayList<String> arket;
        public CustomListAdapter(Context context, ArrayList<String> arid, ArrayList<String> arname, ArrayList<String> aribu,ArrayList<String> ardata, ArrayList<String> arket) {
            super(context, R.layout.list_riwayat, arname);
            // TODO Auto-generated constructor stub

            this.context= (Activity) context;
            this.arid= arid;
            this.arname= arname;
            this.aribu=aribu;
            this.ardata=ardata;
            this.arket=arket;
        }

        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.list_riwayat, null,true);

            TextView txtId = (TextView) rowView.findViewById(R.id.txtid);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.txtjudul);
            TextView ibu = (TextView) rowView.findViewById(R.id.txtibu);
            TextView data = (TextView) rowView.findViewById(R.id.textdata);
            TextView extratxt = (TextView) rowView.findViewById(R.id.textket);

            txtId.setText(arid.get(position));
            txtTitle.setText(arname.get(position));
            ibu.setText(aribu.get(position));
            data.setText(ardata.get(position));
//            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.baby));
            extratxt.setText(arket.get(position));
            return rowView;

        };
    }



