package com.example.ghea.stuntingApp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button IDEN,ABOUT,KELUAR,DATA;
	DBhelper dbhelper ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_menu);

		DATA = (Button) findViewById(R.id.buttondatabaru);
		IDEN = (Button) findViewById(R.id.buttonstart);
		ABOUT = (Button) findViewById(R.id.buttonabout);
		KELUAR = (Button) findViewById(R.id.buttonkeluar);

		DATA.setOnClickListener(new View.OnClickListener()  {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(MainActivity.this, InputData.class);
				startActivity(i);
			}
		});

		IDEN.setOnClickListener(new View.OnClickListener()  {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(MainActivity.this, LoginActivity.class);
				startActivity(i);
			}
		});

		ABOUT.setOnClickListener(new View.OnClickListener()  {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(MainActivity.this, About.class);
				startActivity(i);
			}
		});


		KELUAR.setOnClickListener(new View.OnClickListener()  {

			@Override
			public void onClick(View view) {
				android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
				builder.setTitle("Keluar Aplikasi");
				builder.setMessage("Apakah anda ingin keluar aplikasi?");
				builder.setPositiveButton("Ya",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// TODO Auto-generated method stub
								Intent setIntent = new Intent(Intent.ACTION_MAIN);
								setIntent.addCategory(Intent.CATEGORY_HOME);
								setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(setIntent);
							}
						});
				builder.setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int arg1) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
				builder.create().show();
			}
		});

		dbhelper=new DBhelper(this);

		double[][] acuan = new double[][]
                            {{8.539764661399035, 11.824873443468602, 19.448713166099225, 0},
                             {6.528242690658099, 10.102599831386922, 16.327048026683634, 0},
                             {4.75078854887591,  8.414879965828078,  14.83168033720062,  0},
                             {7.12177775274055,  10.167918625878675, 16.856111245099402, 0},
                             {7.89451853660574,  12.725457752474707, 21.564936754816884, 0},
                             {9.276488761732603, 14.375434906416372, 23.09775839447083,  0},
                             {7.21235186621787,  12.254168519522336, 21.167338022545874, 0},
                             {7.922752884901224, 11.778886171391509, 19.854927303191634, 0},
                             {7.225935249117434, 12.780010638225788, 22.631915693754618, 0},
                             {7.158856664693757, 11.467253097128395, 19.04507929056881,  0},

                             {5.646836482045081, 8.79149594490064,   15.106895188939594, 1},
                             {5.613756921084686, 7.816372796400096,  12.350531050706502, 1},
                             {6.284327861766682, 9.227727369426988,  15.190386544065753, 1},
                             {6.002569487666792, 9.023035523499829,  15.633628336256736, 1},
                             {3.8246744756899496,5.84939056840384,   10.071826195522672, 1},
                             {5.8443707828270455,7.737312874998413,  12.125266479065404, 1},
                             {6.765817379487609, 10.846688993034894, 18.609226265119887, 1},
                             {7.012198764673366, 10.335690914272178, 16.575805382837956, 1},
                             {6.14142909508825,  8.7961119660284,    13.969054748744581, 1},
                             {6.3826621139284905,9.269232224033232,  15.029334901345877, 1},

                             {4.986598818866935, 9.436521187371943,  15.48025359621697,  2},
                             {7.717780469636062, 13.758289757732151, 21.88659913159608,  2},
                             {7.086132263000745, 13.80535799764415,  22.99787458739309,  2},
                             {6.74633092818927,  13.1309897988717331,21.775559944957575, 2},
                             {7.0521057689686195,13.410183620055072, 21.30732162817961,  2},
                             {5.161393031958767, 10.940744996200102, 17.697162502386192, 2},
                             {6.728195437158053, 12.885219446857437, 21.173615238598398, 2},
                             {6.408862560866882, 11.831244427679858, 18.9898515191002,   2},
                             {6.673878073982228, 15.598188055383345, 25.88235751188262,  2},
                             {9.315364345652304, 16.87085444041485,  26.255435526216242, 2},

                             {6.52668473916519,  10.112582977096583, 16.242305286205816, 3},
                             {5.789311363993318, 8.506669688878736,  13.511518911432528, 3},
                             {8.52005041284118,  14.414162140546281, 22.714977333888378, 3},
                             {7.2432492958595684,10.250208873461442, 16.713306746828138, 3},
                             {8.83068391562294,  13.974016150296638, 21.2938216875412,   3},
                             {5.807567000588026, 9.399939254919204,  15.09876902919119,  3},
                             {10.309012166483756,14.10112999779159,  20.845166869806654, 3},
                             {7.242831001980309, 10.913195294842101, 17.176301720699477, 3},
                             {7.97114647047445,  12.580727516492018, 20.171892196556854, 3},
                             {5.802608143979899, 9.346545988486929,  15.062090792597559, 3},
                             {6.202965223765048, 9.798270715282557,  16.073541806496575, 3},

                             {8.296154441349428,  14.07847802537914,  17.894488056047944, 4},
                             {12.431524168284227, 20.819683786945905, 25.76114448229417,  4},
                             {12.942108251132897, 21.227511886538053, 26.94986533659094,  4},
                             {9.730198477823643,  16.857176208947326, 21.657650397491746, 4},
                             {12.672357861817648, 21.12944885092536,  26.738788478913904, 4},
                             {10.319172354176928, 18.08816729823997 , 23.520836893262437, 4},
                             {10.264811726055092, 17.862195265773735, 23.390091820402663, 4},
                             {10.203754787984678, 17.597493448020966, 22.91750218399301,  4},
                             {8.065653514768067,  13.903068776340007, 18.14555227023036,  4},
                             {7.996034872479434,  15.417033362118497, 20.33318596706106,  4},

                             {7.0239357982554145, 9.712850563116682,  16.17363667934799,  5},
                             {6.0705205025678834, 7.941878397878914,  12.661693992955058, 5},
                             {6.650739170888039,  10.105741066198009, 16.39383162755436,  5},
                             {6.0171610523973555, 8.881164584997146,  14.385971025092154, 5},
                             {6.950254673107996,  11.10825042445518,  18.387239277893062, 5},
                             {5.711868320243489,  8.376522241074708,  13.318254648498312, 5},
                             {8.839597693674946,  10.5142116443404,   14.348142367884858, 5},
                             {6.833887657697215,  10.08358773479555,  14.396615625032412, 5},
                             {8.520182707352497,  9.822025216982523,  14.171447766325436, 5},
                            };
					if (dbhelper.getAlldata().size()==0){
						for(int ulang = 0; ulang < 59;ulang++){

							Data Data = new Data();
							Data.setR(String.valueOf(acuan[ulang][0]));
							Data.setG(String.valueOf(acuan[ulang][1]));
							Data.setB(String.valueOf(acuan[ulang][2]));
							Data.setKlasifikasi(String.valueOf(acuan[ulang][3]));
							dbhelper.adddata(Data);

						}
					}else{

					}

	}
}
