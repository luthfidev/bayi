package com.example.ghea.stuntingApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ghea.stuntingApp.api.BaseApiService;
import com.example.ghea.stuntingApp.api.UtilsApi;
import com.example.ghea.stuntingApp.model.ResponseMedis;
import com.example.ghea.stuntingApp.model.SemuamedisItem;
import com.example.ghea.stuntingApp.utils.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.OpenCVLoader;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Identifikasilama extends AppCompatActivity {



    SharedPrefManager sharedPrefManager;
    ImageView ivPickedImage;
    Button fab, btnlogout, btnInputdata;
    ArrayList<Double> jarak = new ArrayList<Double>();
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private EditText date;
    DatePickerDialog datePickerDialog;
    Spinner inpjenis, inpmedis;
    int sukses, tahunLahir;
    double out;
    ProgressDialog pDialog, loading;
    EditText inpberat,inpumur,inpnama,inpibu, inptgllahir, inpttinggi;
    //    ArrayList<Data> listData;
//    DBhelper databaseHelper;
    private Uri mHighQualityImageUri = null;
    String hasilstunting,hasilgizi,nama,ibu,umur, umurs ,jenis,berat, tgllahir, tinggi, status_gizi, status_stunting, nomedis;
    double[][] lakigizi,lakistunting,cewekgizi,cewekstunting;

    ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
    //    JSONArray college = null;
    HashMap<String, String> map;
    Context mContext;
    BaseApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_main);
        sharedPrefManager = new SharedPrefManager(this);

        final Calendar newCalendar = Calendar.getInstance();

        mContext = this;
        mApiService = UtilsApi.getAPIService();




        pDialog = new ProgressDialog(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(!OpenCVLoader.initDebug()){
            Log.d("CVerror","OpenCV library Init failure");
        }else{
            showIntro();
            // selectImage();
            selectForm();
            // ivPickedImage = (ImageView) findViewById(R.id.ivImage);
//            databaseHelper = new DBhelper(this);

            lakigizi = new double[][]
                    {
                            {0, 1.9,2.3,4.2,4.3},
                            {1, 2.1,2.8,5.5,5.6},
                            {2, 2.5,3.4,6.7,6.8},
                            {3, 3.0,4.0,7.6,7.7},
                            {4, 3.6,4.6,8.4,8.5},
                            {5, 4.2,5.2,9.1,9.2},
                            {6, 4.8,5.8,9.7,9.8},
                            {7, 5.3,6.3,10.2,10.3},
                            {8, 5.8,6.8,10.7,10.8},
                            {9, 6.2,7.1,11.2,11.3},
                            {10, 6.5,7.5,11.6,11.7},
                            {11, 6.8,7.8,11.9,12.0},
                            {12, 7.0,8.0,12.3,12.4},
                            {13, 7.2,8.2,12.6,12.7},
                            {14, 7.4,8.4,12.9,13.0},
                            {15, 7.5,8.6,13.1,13.2},
                            {16, 7.6,8.7,13.4,13.5},
                            {17, 7.7,8.9,13.6,13.7},
                            {18, 7.8,9.0,13.8,13.9},
                            {19, 7.9,9.1,14.0,14.1},
                            {20, 8.0,9.3,14.3,14.4},
                            {21, 8.2,9.4,14.5,14.6},
                            {22, 8.3,9.6,14.7,14.8},
                            {23, 8.4,9.7,14.9,15.0},
                            {24, 8.9,10.0,15.6,15.7},
                            {25, 8.9,10.1,15.8,15.9},
                            {26, 9.0,10.2,16.0,16.1},
                            {27, 9.0,10.3,16.2,16.3},
                            {28, 9.1,10.4,16.5,16.6},
                            {29, 9.2,10.5,16.7,16.8},
                            {30, 9.3,10.6,16.9,17.0},
                            {31, 9.3,10.8,17.1,17.2},
                            {32, 9.4,10.9,17.3,17.4},
                            {33, 9.5,11.0,17.5,17.6},
                            {34, 9.6,11.1,17.7,17.8},
                            {35, 9.6,11.2,17.9,18.0},
                            {36, 9.7,11.3,18.2,18.3},
                            {37, 9.8,11.4,18.4,18.5},
                            {38, 9.9,11.6,18.6,18.7},
                            {39, 10.0,11.7,18.8,18.9},
                            {40, 10.1,11.8,19.0,19.1},
                            {41, 10.2,11.9,19.2,19.3},
                            {42, 10.3,12.0,19.4,19.5},
                            {43, 10.4,12.2,19.6,19.7},
                            {44, 10.5,12.3,19.8,19.9},
                            {45, 10.6,12.4,20.0,20.1},
                            {46, 10.7,12.5,20.3,20.4},
                            {47, 10.8,12.7,20.5,20.6},
                            {48, 10.9,12.8,20.7,20.8},
                            {49, 11.0,12.9,20.9,21.0},
                            {50, 11.1,13.0,21.1,21.2},
                            {51, 11.2,13.2,21.3,21.4},
                            {52, 11.3,13.3,21.6,21.7},
                            {53, 11.4,13.4,21.8,21.9},
                            {54, 11.5,13.6,22.0,22.1},
                            {55, 11.7,13.7,22.2,22.3},
                            {56, 11.8,13.8,22.5,22.6},
                            {57, 11.9,14.0,22.7,22.8},
                            {58, 12.0,14.1,22.9,23.0},
                            {59, 12.1,14.2,23.2,23.3},
                    };
            cewekgizi = new double[][]
                    {
                            {0, 1.7,2.1,3.9,4.0},
                            {1, 2.1,2.7,5.0,5.1},
                            {2, 2.6,3.2,6.0,6.1},
                            {3, 3.1,3.8,6.9,7.0},
                            {4, 3.6,4.4,7.6,7.7},
                            {5, 4.0,4.9,8.3,8.4},
                            {6, 4.5,5.4,8.9,9.0},
                            {7, 4.9,5.8,9.5,9.6},
                            {8, 5.3,6.2,10.0,10.1},
                            {9, 5.6,6.5,10.4,10.5},
                            {10, 5.8,6.8,10.8,10.9},
                            {11, 6.1,7.1,11.2,11.3},
                            {12, 6.3,7.3,11.5,11.6},
                            {13, 6.5,7.5,11.8,11.9,},
                            {14, 6.6,7.7,12.1,12.2},
                            {15, 6.8,7.9,12.3,12.4},
                            {16, 6.9,8.1,12.5,12.6},
                            {17, 7.1,8.2,12.8,12.9},
                            {18, 7.2,8.4,13.0,13.1},
                            {19, 7.4,8.5,13.2,13.3},
                            {20, 7.5,8.7,13.4,13.5},
                            {21, 7.6,8.9,13.7,13.8},
                            {22, 7.8,9.0,13.9,14.0},
                            {23, 8.0,9.2,14.1,14.2},
                            {24, 8.2,9.3,14.5,14.6},
                            {25, 8.3,9.5,14.8,14.9},
                            {26, 8.4,9.7,15.1,15.2},
                            {27, 8.6,9.8,15.5,15.6},
                            {28, 8.7,10.0,15.8,15.9},
                            {29, 8.8,10.1,16.0,16.1},
                            {30, 8.9,10.2,16.3,16.4},
                            {31, 9.0,10.4,16.6,16.7},
                            {32, 9.1,10.5,16.9,17.0},
                            {33, 9.3,10.7,17.1,17.2},
                            {34, 9.4,10.8,17.4,17.5},
                            {35, 9.5,10.9,17.7,17.8},
                            {36, 9.6,11.1,17.9,18.0},
                            {37, 9.7,11.2,18.2,18.3},
                            {38, 9.8,11.3,18.4,18.5},
                            {39, 9.9,11.4,18.6,18.7},
                            {40, 10.0,11.5,18.9,19.0},
                            {41, 10.1,11.7,19.1,19.2},
                            {42, 10.2,11.8,19.3,19.4},
                            {43, 10.3,11.9,19.5,19.6},
                            {44, 10.4,12.0,19.7,19.8},
                            {45, 10.5,12.1,20.0,20.1},
                            {46, 10.6,12.2,20.2,20.3},
                            {47, 10.7,12.4,20.4,20.5},
                            {48, 10.8,12.5,20.6,20.7},
                            {49, 10.8,12.6,20.8,20.9},
                            {50, 10.9,12.7,21.0,21.1},
                            {51, 11.0,12.8,21.2,21.3},
                            {52, 11.1,12.9,21.4,21.5},
                            {53, 11.2,13.0,21.6,21.7},
                            {54, 11.3,13.1,21.8,21.9},
                            {55, 11.4,13.2,22.1,22.2},
                            {56, 11.4,13.3,22.3,22.4},
                            {57, 11.5,13.4,22.5,22.6},
                            {58, 11.6,13.5,22.7,22.8},
                            {59, 11.7,13.6,22.9,23.0},
                    };
            lakistunting = new double[][]
                    {
                            {24, 79.1, 92.0},
                            {25, 79.8, 92.9},
                            {26, 80.5, 93.9},
                            {27, 81.2, 94.8},
                            {28, 81.9, 95.7},
                            {39, 82.6, 96.6},
                            {30, 83.3, 97.5},
                            {31, 84.0, 98.3},
                            {32, 84.6, 99.2},
                            {33, 85.3, 100.1},
                            {34, 85.9, 100.9},
                            {35, 86.6, 101.7},
                            {36, 87.2, 102.5},
                            {37, 87.8, 103.3},
                            {38, 88.5, 104.1},
                            {39, 89.1, 104.9},
                            {40, 89.7, 105.7},
                            {41, 90.3, 106.4},
                            {42, 90.9, 107.2},
                            {43, 91.5, 107.9},
                            {44, 92.0, 108.7},
                            {45, 92.6, 109.4},
                            {46, 93.2, 110.1},
                            {47, 93.8, 110.8},
                            {48, 94.3, 111.5},
                            {49, 94.9, 112.2},
                            {50, 95.4, 112.8},
                            {51, 96.0, 113.5},
                            {52, 96.5, 114.2},
                            {53, 97.0, 114.8},
                            {54, 97.6, 115.4},
                            {55, 98.1, 116.1},
                            {56, 98.6, 116.7},
                            {57, 99.1, 117.3},
                            {58, 99.6, 117.9},
                            {59, 100.1, 114.5},
                            {60, 100.6, 119.1},
                    };
            cewekstunting = new double[][]
                    {
                            {24, 78.0, 90.9},
                            {25, 78.7, 91,9},
                            {26, 79.5, 92.8},
                            {27, 80.2, 93.8},
                            {28, 80.9, 94.7},
                            {39, 81.7, 95.6},
                            {30, 82.4, 96.5},
                            {31, 83.1, 97.3},
                            {32, 83.7, 98.2},
                            {33, 84.4, 99.0},
                            {34, 85.1, 99.8},
                            {35, 85.7, 100.6},
                            {36, 86.4, 101.4},
                            {37, 87.0, 102.1},
                            {38, 87.6, 102.9},
                            {39, 88.3, 103.6},
                            {40, 88.9, 104.3},
                            {41, 89.5, 105.0},
                            {42, 90.1, 105.7},
                            {43, 90.6, 106.4},
                            {44, 91.2, 107.1},
                            {45, 91.8, 107.8},
                            {46, 92.3, 108.4},
                            {47, 92.9, 109.1},
                            {48, 93.4, 109.7},
                            {49, 94.0, 110.4},
                            {50, 94.5, 111.0},
                            {51, 95.0, 111.6},
                            {52, 95.5, 112.3},
                            {53, 96.0, 112.9},
                            {54, 96.6, 113.5},
                            {55, 97.0, 114.1},
                            {56, 97.5, 114.8},
                            {57, 98.0, 115.4},
                            {58, 98.5, 116.0},
                            {59, 99.0, 116.6},
                            {60, 99.4, 117.2},
                    };




        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void initSpinnerMedis(){
        loading = ProgressDialog.show(mContext, null, "harap tunggu...", true, false);

        mApiService.getSemuaMedis().enqueue(new Callback<ResponseMedis>() {
            @Override
            public void onResponse(Call<ResponseMedis> call, Response<ResponseMedis> response) {
                if (response.isSuccessful()) {
                    loading.dismiss();
                    List<SemuamedisItem> semuamedisItems = response.body().getSemuamedis();
                    List<String> listSpinner = new ArrayList<String>();
                    for (int i = 0; i < semuamedisItems.size(); i++){
                        listSpinner.add(semuamedisItems.get(i).getNo_medis());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                            android.R.layout.simple_spinner_item, listSpinner);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    inpmedis.setAdapter(adapter);
                } else {
                    loading.dismiss();
                    Toast.makeText(mContext, "Gagal mengambil data dosen", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseMedis> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void selectForm()
    {
        final Calendar newCalendar = Calendar.getInstance();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Identifikasilama.this);
        LayoutInflater inflater = Identifikasilama.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.input_lama, null);

        dialogBuilder.setView(dialogView);

        initSpinnerMedis();
        inpmedis = (Spinner) dialogView.findViewById(R.id.inpmedis);
        inpmedis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedName = parent.getItemAtPosition(position).toString();
//                requestDetailDosen(selectedName);
                Toast.makeText(mContext, "Kamu memilih No Medis " + selectedName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //inpnama = (EditText) dialogView.findViewById(R.id.inptgllahirlama);
        //inptgllahir = (EditText) dialogView.findViewById(R.id.inptgllahir);
        inpttinggi =(EditText) dialogView.findViewById(R.id.inpttinggilama);
        inpberat = (EditText) dialogView.findViewById(R.id.inptgl1lama);
        inpjenis = (Spinner) dialogView.findViewById(R.id.spinner);
        // eText = (EditText) findViewById(R.id.inptgllahir);

        inptgllahir = (EditText) dialogView.findViewById(R.id.inptgllahirlama);
        inptgllahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(Identifikasilama.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        inptgllahir.setText(dateFormatter.format(newDate.getTime()));

                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

        dialogBuilder.setTitle("Input Data");
        dialogBuilder.setMessage("Masukan Data Balita");
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                Calendar now =  Calendar.getInstance();

                String tgllahir = inptgllahir.getText().toString();
                String jnskel = inpjenis.getSelectedItem().toString();
                String berat = inpberat.getText().toString();
                String tinggi = inpttinggi.getText().toString();


                String lastFourDigits = "";

                if (tgllahir.length() > 4){
                    lastFourDigits = tgllahir.substring(tgllahir.length() - 4);
                } else {
                    lastFourDigits = tgllahir;
                }

                int tgllahirx = Integer.parseInt(lastFourDigits);
                int xumur = ((now.get(Calendar.YEAR) - tgllahirx) * 12);
                //int xumur = xxumur * 12;

                // String umurs = "mur";
                String umur = String.valueOf(xumur);
                //inpumur.setText(umur);
                //String umurs = inpumur.getText().toString();
                // String umur = inpumur.setText(String.valueOf(xumur));

               // String umurs = inpumur.setText(umur);

                // String umur = xumur.getText().toString();
                // Toast.makeText(getApplicationContext(),"Tinggi : "+ String.valueOf(out), Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),"Tinggi : "+ String.valueOf(tinggi), Toast.LENGTH_LONG).show();

                if (Integer.parseInt(umur)<60) {
                    if (Integer.parseInt(umur) > 24 && Integer.parseInt(umur) < 60) {
                        if (jnskel.equals("Perempuan")) {
                            for (int a = 0; a < cewekstunting.length; a++) {
                                Log.d(" Cek Log: ", String.valueOf(jnskel + " " + cewekstunting[a][0]));
                                if (String.valueOf(umur).equals(String.valueOf(Math.round(cewekstunting[a][0])))) {
                                    //do something for equals
                                    double cek = cewekstunting[a][1];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if ((Integer.parseInt(tinggi)) < cek) {
                                        hasilstunting = "Stunting";
                                    } else {
                                        hasilstunting = "Normal";
                                    }
                                }
                            }
                            for (int a = 0; a < cewekgizi.length; a++) {
                                Log.d(" Cek Log: ", String.valueOf(jnskel + " " + cewekgizi[a][0]));
                                if ((umur).equals(String.valueOf(Math.round(cewekgizi[a][0])))) {
                                    //do something for equals
                                    double cek1 = cewekgizi[a][1];
                                    double cek2 = cewekgizi[a][2];
                                    double cek3 = cewekgizi[a][3];
                                    double cek4 = cewekgizi[a][4];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if (Double.parseDouble(berat) < cek1) {
                                        hasilgizi = "Gizi Buruk";
                                    } else if (Double.parseDouble(berat) < cek2) {
                                        hasilgizi = "Gizi Kurang";
                                    } else if (Double.parseDouble(berat) < cek3) {
                                        hasilgizi = "Gizi Baik";
                                    } else {
                                        hasilgizi = "Gizi Lebih";
                                    }
                                }
                            }
                        }else {
                            for (int a = 0; a < lakistunting.length; a++) {
                                Log.d(" Cek Log stun: ", String.valueOf(jnskel + " " + lakistunting[a][0]));
                                if ((umur).equals(String.valueOf(Math.round(lakistunting[a][0])))) {
                                    //do something for equals
                                    double cek = lakistunting[a][1];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if (out < cek) {
                                        hasilstunting = "Stunting";

                                    } else {
                                        hasilstunting = "Normal";
                                    }
                                    Log.d(" Cek Log: ", String.valueOf(jnskel + " " + hasilstunting));
                                }
                            }
                            for (int a = 0; a < lakigizi.length; a++) {
                                Log.d(" Cek Log gizi: ", String.valueOf(jnskel + " " + lakigizi[a][0]));
                                if ((umur).equals(String.valueOf(Math.round(lakigizi[a][0])))) {
                                    //do something for equals
                                    double cek1 = lakigizi[a][1];
                                    double cek2 = lakigizi[a][2];
                                    double cek3 = lakigizi[a][3];
                                    double cek4 = lakigizi[a][4];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if (Double.parseDouble(berat) < cek1) {
                                        hasilgizi = "Gizi Buruk";
                                    } else if (Double.parseDouble(berat) < cek2) {
                                        hasilgizi = "Gizi Kurang";
                                    } else if (Double.parseDouble(berat) < cek3) {
                                        hasilgizi = "Gizi Baik";
                                    } else {
                                        hasilgizi = "Gizi Lebih";
                                    }
                                    Log.d(" Cek Log: ", String.valueOf(jnskel + " " + hasilgizi));
                                }
                            }
                        }
                    }else {
                        if (jnskel.equals("Perempuan")) {
                            for (int a = 0; a < cewekgizi.length; a++) {
                                Log.d(" Cek Log: ", String.valueOf(jnskel + " " + cewekgizi[a][0]));
                                if ((umur).equals(String.valueOf(Math.round(cewekgizi[a][0])))) {
                                    //do something for equals
                                    double cek1 = cewekgizi[a][1];
                                    double cek2 = cewekgizi[a][2];
                                    double cek3 = cewekgizi[a][3];
                                    double cek4 = cewekgizi[a][4];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if (Double.parseDouble(berat) < cek1) {
                                        hasilgizi = "Gizi Buruk";
                                    } else if (Double.parseDouble(berat) < cek2) {
                                        hasilgizi = "Gizi Kurang";
                                    } else if (Double.parseDouble(berat) < cek3) {
                                        hasilgizi = "Gizi Baik";
                                    } else {
                                        hasilgizi = "Gizi Lebih";
                                    }
                                }
                            }
                        }else {
                            for (int a = 0; a < lakigizi.length; a++) {
                                Log.d(" Cek Log: ", String.valueOf(jnskel + " " + lakigizi[a][0]));
                                if ((umur).equals(String.valueOf(Math.round(lakigizi[a][0])))) {
                                    //do something for equals
                                    double cek1 = lakigizi[a][1];
                                    double cek2 = lakigizi[a][2];
                                    double cek3 = lakigizi[a][3];
                                    double cek4 = lakigizi[a][4];
//                                            Toast.makeText(getApplicationContext(), String.valueOf(out)+"Cewek 0-2 \n"+ String.valueOf(cewek01[a][1]) , Toast.LENGTH_LONG).show();
                                    if (Double.parseDouble(berat) < cek1) {
                                        hasilgizi = "Gizi Buruk";
                                    } else if (Double.parseDouble(berat) < cek2) {
                                        hasilgizi = "Gizi Kurang";
                                    } else if (Double.parseDouble(berat) < cek3) {
                                        hasilgizi = "Gizi Baik";
                                    } else {
                                        hasilgizi = "Gizi Lebih";
                                    }
                                }
                            }
                        }
                    }
                }

                if (hasilstunting==null){
                    hasilstunting="";

                }
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Identifikasilama.this);
                alertDialogBuilder.setTitle("Hasil Analisa Kondisi Bayi");
                alertDialogBuilder.setMessage(hasilstunting+" "+hasilgizi+"\n Umur : "+umur);


                alertDialogBuilder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        new Simpan().execute();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

//                    System.out.println("hasil ="+hasil);


            }
        }).setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
//								getActivity().finish();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();


    }



    private void showIntro(){
        //  Declare a new thread to do a preference check
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    Intent i = new Intent(Identifikasilama.this, PringIntro.class);
                    startActivity(i);

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        // Start the thread
        t.start();
    }

    public class Simpan extends AsyncTask<String, String, String> {

        ArrayList<HashMap<String, String>> contactList = new ArrayList<HashMap<String, String>>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Proses Menyimpan Data");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

            //nama = inpnama.getText().toString();
            //ibu = inpibu.getText().toString();
            nomedis = inpmedis.getSelectedItem().toString();
           // tgllahir = inptgllahir.getText().toString();
            //umur = inpumur.getText().toString();

            // umur = umurs;
            berat = inpberat.getText().toString();
            jenis = inpjenis.getSelectedItem().toString();
            //ket = hasilstunting+" "+hasilgizi;
            status_gizi = hasilgizi;
            status_stunting = hasilstunting;

            tinggi = inpttinggi.getText().toString();

        }

        @Override
        protected String doInBackground(String... arg0) {

           // String url = "http://localhost/stunting/api/api_insert_lama.php";
           String url = "https://luthfidev.online/ghea/api/api_insert_lama.php";
            JSON jParser = new JSON();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
          //  params.add(new BasicNameValuePair("nama_anak", nama));
           // params.add(new BasicNameValuePair("nama_ibu", ibu));

            params.add(new BasicNameValuePair("no_medisanak", nomedis));
           // params.add(new BasicNameValuePair("tanggal_lahir", tgllahir));
            params.add(new BasicNameValuePair("usia", umur));
            params.add(new BasicNameValuePair("bb_anak", berat));
            params.add(new BasicNameValuePair("tb_anak", String.valueOf(tinggi)));
            params.add(new BasicNameValuePair("jenis_kelamin", jenis));
            //params.add(new BasicNameValuePair("keterangan", ket));
            params.add(new BasicNameValuePair("status_stunting", status_stunting));
            params.add(new BasicNameValuePair("status_gizi", status_gizi));
//            params.add(new BasicNameValuePair("latlong", loc));
            JSONObject json = jParser.makeHttpRequest(url, "POST", params);
            Log.d(" Cek JSON1: ", json.toString());

            try {
                sukses = json.getInt("status");
                //int sukses1 = json1.getInt(TAG_MEMBER);

                if (sukses  == 1) {
                    Log.d("Berhasil daftar",json.toString());
                    return json.getString("messages");

                } else {
                    // jika gagal dalam menambah data
                    Log.d("Gagal daftar ",json.toString());
                    return json.getString("messages");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pDialog.dismiss();
            if (sukses  == 1) {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(Identifikasilama.this);
                alertDialogBuilder.setMessage("Data Berhasil Disimpan");

                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent i = new Intent(getApplicationContext(), com.example.ghea.stuntingApp.Menu.class);
                        startActivity(i);

                        finish();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            } else {
                // jika gagal dalam menambah data
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(Identifikasilama.this);
                alertDialogBuilder.setMessage("Penyimpanan gagal");

                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }

    }
}
