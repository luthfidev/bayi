package com.example.ghea.stuntingApp;

public class Data {
    private String R;
    private String G;
    private String B;
    private String Klasifikasi;
//    private Blob gambar;
    private byte[] gambar2;

    public Data(String R, String G, String B, String klasifikasi) {
        this.R = R;
        this.G = G;
        this.B = B;
        this.Klasifikasi = klasifikasi;
    }

    public Data() {

    }

    public String getR() {
        return R;
    }

    public void setR(String R) {
        this.R = R;
    }

    public String getB() {
        return B;
    }

    public void setB(String B) {
        this.B = B;
    }

    public String getG() {
        return G;
    }

    public void setG(String G) {
        this.G = G;
    }

    public String getKlasifikasi() {
        return Klasifikasi;
    }

    public void setKlasifikasi(String Klasifikasi) {
        this.Klasifikasi = Klasifikasi;
    }




}
