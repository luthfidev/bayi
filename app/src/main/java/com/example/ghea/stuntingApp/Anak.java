package com.example.ghea.stuntingApp;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Anak extends ListActivity {

	ListView mylistview;
	JSONArray contacts = null;
	public ProgressBar pg;
	ProgressDialog pDialog;
	JSONArray cek = null;
	String pesan, id,nama, ibu;
	int sukses;
//	SessionManager session;
	ArrayList<String> arid = new ArrayList<String>();
	ArrayList<String> arnama = new ArrayList<String>();
	ArrayList<String> aribu = new ArrayList<String>();
	ArrayList<String> ardata = new ArrayList<String>();
	ArrayList<String> arket = new ArrayList<String>();
	EditText inpnama,inpibu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.riwayat);
		pDialog = new ProgressDialog(this);
		mylistview = getListView();
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Anak.this);
		LayoutInflater inflater = Anak.this.getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.input_cari, null);
		dialogBuilder.setView(dialogView);

		inpnama = (EditText) dialogView.findViewById(R.id.inpnama);
//		inpibu = (EditText) dialogView.findViewById(R.id.inpibu);


		dialogBuilder.setTitle("Input Data");
		dialogBuilder.setMessage("Masukan Data Balita");
		dialogBuilder.setPositiveButton("Cari", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//do something with edt.getText().toString();
				new AmbilData().execute();

			}
		}).setNegativeButton("Batal", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Intent i = new Intent(Anak.this, com.example.ghea.stuntingApp.Menu.class);
				startActivity(i);
				//pass
//				getActivity().finish();
			}
		});
		AlertDialog b = dialogBuilder.create();
		b.show();


	}

	public class AmbilData extends AsyncTask<String, String, String> {

		ArrayList<HashMap<String, String>> contactList = new ArrayList<HashMap<String, String>>();

		String nama, harga;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog.setMessage("Loading Data ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
//			pg.setVisibility(View.VISIBLE);
			nama = inpnama.getText().toString();
//			ibu = inpibu.getText().toString();
		}

		@Override
		protected String doInBackground(String... arg0) {

            //String url = "http://brilliantcode.space/GHEA/JSON/data.php?act=carianak";
           String url = "https://luthfidev.online/ghea/api/api_search.php";

            JSON jParser = new JSON();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("no_medis", nama));
            /*params.add(new BasicNameValuePair("nama_anak", nama));
            params.add(new BasicNameValuePair("nama_ibu", ibu));*/


            JSONObject json = jParser.makeHttpRequest(url, "POST", params);
            Log.d(" Cek JSON1: ", json.toString());



            try {


                contacts = json.getJSONArray("data");

                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    ;

                    String id_1 = c.getString("no_medis").trim();
                    String nama_1 = c.getString("nama_anak").trim();
                    String jenis_ = c.getString("jk").trim();
                    String ibu_1 = c.getString("nama_ibu").trim();
                    String umur_1 = c.getString("tanggal_lahir").trim();
                    String berat_1 = c.getString("berat").trim();
                    String tinggi_1 = c.getString("tinggi").trim();
                    String ket_1 = c.getString("keterangan").trim();
                    String tgl_1 = c.getString("tanggal").trim();

                    map.put("no_medis", id_1);
                    map.put("nama_anak", "Nama : " + nama_1 + "\nIbu : " + ibu_1);
                    map.put("data", "Jenis kelamin : " + jenis_ + "\nBerat : " + berat_1 + "\nTinggi : " + tinggi_1 + "\nTanggal Lahir : " + umur_1 + "\ntanggal : " + tgl_1);
                    map.put("keterangan", ket_1);


                    arid.add(id_1);
                    arnama.add("Nama : " + nama_1);
                    aribu.add("Ibu : " + ibu_1);
                    ardata.add("Jenis kelamin : " + jenis_ + "\nBerat : " + berat_1 + "\nTinggi : " + tinggi_1 + "\nTanggal Lahir : " + umur_1 + "\ntanggal : " + tgl_1);
                    arket.add(ket_1);

                    contactList.add(map);
                    sukses = json.getInt("status");
                    //int sukses1 = json1.getInt(TAG_MEMBER);

                    if (sukses  == 1) {
                        Log.d("Data Ditemukan",json.toString());
                        return json.getString("messages");

                    } else {
                        // jika gagal dalam melihat data
                        Log.d("Data Tidak Ditemukan ",json.toString());
                        return json.getString("messages");
                    }
                }


            } catch (JSONException e) {


            }

            return null;

            /*String error_message = jsonRESULTS.getString("message");
            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();*/
        }

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
//			pg.setVisibility(View.GONE);

			CustomListAdapter adapter=new CustomListAdapter(Anak.this, arid,arnama, aribu,ardata,arket);
//			list=(ListView)findViewById(R.id.list);
			mylistview.setAdapter(adapter);


            if (sukses  == 1) {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(Anak.this);
                alertDialogBuilder.setMessage("Data Ditemukan");

                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        //Intent i = new Intent(getApplicationContext(),Anak.class);
                        //startActivity(i);

                        //finish();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            } else {
                // jika gagal dalam menambah data
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(Anak.this);
                alertDialogBuilder.setMessage("Data Tidak Ditemukan");

                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                    }
                });

                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
//			ListAdapter adapter2 = new SimpleAdapter(getActivity().getApplicationContext(),
//					contactList, R.layout.list_riwayat,
//					new String[]{"nama", "alamat", "id"}, new int[]{ R.id.txtjudul, R.id.textberat, R.id.txtid
//					});
//
//			setListAdapter(adapter2);

		}

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String latlng = ((TextView) v.findViewById(R.id.txtjudul)).getText().toString();
	}

        }