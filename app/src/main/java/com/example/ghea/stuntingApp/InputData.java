package com.example.ghea.stuntingApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class InputData extends AppCompatActivity {

    ImageView ivPickedImage;
    TextView hasil,hasilrgb,hasilr,hasilg,hasilb;
    Button fab;
    ArrayList<Double> jarak = new ArrayList<Double>();
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect;
    private String userChoosenTask;
    double hasiltemp;
    double hasilakhir;
    Spinner spinner;
    DBhelper dbhelper ;

    ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
    //    JSONArray college = null;
    HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(!OpenCVLoader.initDebug()){
            Log.d("CVerror","OpenCV library Init failure");
        }else{

            selectImage();
            ivPickedImage = (ImageView) findViewById(R.id.ivImage);
//            hasil = (TextView) findViewById(R.id.texthasil);
//            hasilrgb = (TextView) findViewById(R.id.texthasilrgb);
//            hasilr = (TextView) findViewById(R.id.texthasilr);
//            hasilg = (TextView) findViewById(R.id.texthasilg);
//            hasilb = (TextView) findViewById(R.id.texthasilb);
            spinner = (Spinner) findViewById(R.id.spinner);

            dbhelper=new DBhelper(this);
            fab = (Button) findViewById(R.id.btnIdentPhoto);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ivPickedImage.buildDrawingCache();
                    Bitmap icon = ivPickedImage.getDrawingCache();

                    Mat image = new Mat();
                    Utils.bitmapToMat(icon, image);
                    ArrayList<Mat> gg = new ArrayList<>();
                    Core.split(image, gg);
                    Mat red = gg.get(0);
                    Mat green = gg.get(1);
                    Mat blue = gg.get(2);

                    //grayscaling
                    Mat grayimage = new Mat();
                    Imgproc.cvtColor(image, grayimage, Imgproc.COLOR_RGB2GRAY);

                    //tressholding
                    Mat tressimage = new Mat();
                    Imgproc.adaptiveThreshold(grayimage, tressimage, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 395, 10);

                    Mat resmask = new Mat();
                    image.copyTo(resmask, tressimage);
                    Mat resmask2 = resmask;

                    ArrayList<Mat> ggb = new ArrayList<>();
                    Core.split(resmask2, ggb);
                    Mat red2 = gg.get(0);
                    Mat green2 = gg.get(1);
                    Mat blue2 = gg.get(2);

                    Bitmap bm = Bitmap.createBitmap(resmask.cols(), resmask.rows(), Bitmap.Config.ARGB_8888);
                    Bitmap bmp32 = bm.copy(Bitmap.Config.ARGB_8888, true);
                    Utils.matToBitmap(resmask2, bmp32);
                    ivPickedImage.setImageBitmap(bmp32);
                    //RemaskRed
                    Mat resmaskr = new Mat();
                    red.copyTo(resmaskr, tressimage);
                    //RemaskGreen
                    Mat resmaskg = new Mat();
                    green.copyTo(resmaskg, tressimage);
                    //RemaskBlue
                    Mat resmaskb = new Mat();
                    blue.copyTo(resmaskb, tressimage);

                    double redColors = 0;
                    double greenColors = 0;
                    double blueColors = 0;
                    int pixelCount = 0;
                    double[] temp;
                    double[] tempr;
                    double[] tempg;
                    double[] tempb;

                    for (int y = 0; y < resmask2.height(); y++) {
                        for (int x = 0; x < resmask2.width(); x++) {
                            pixelCount++;
                            temp = resmask2.get(y, x);
                            //int c = bmp32.getPixel(x, y);
                            if (temp[0] != 0) {

                                pixelCount++;
                                tempr = red2.get(y, x);
                                redColors += tempr[0];
                                tempg = green2.get(y, x);
                                greenColors += tempg[0];
                                tempb = blue2.get(y, x);
                                blueColors += tempb[0];
                            }
                        }
                    }

                    double red1 = (redColors / pixelCount);
                    double green1 = (greenColors / pixelCount);
                    double blue1 = (blueColors / pixelCount);

                    String text = spinner.getSelectedItem().toString();
                    if (text.equals("Kualitas Nomor 1 (Babj)")) {
                        hasilakhir = 0;
                    } else if (text.equals("Kualitas Nomor 2 (Bcdj)")) {
                        hasilakhir = 1;
                    } else if (text.equals("Kualitas Nomor 3 (Yabj")) {
                        hasilakhir = 2;
                    } else if (text.equals("Kualitas Nomor 4 (Ycdj)")) {
                        hasilakhir = 3;
                    } else if (text.equals("Kualitas Nomor 5 (Labj)")) {
                        hasilakhir = 4;
                    } else if (text.equals("Kualitas Nomor 6 (Lcdj)")) {
                        hasilakhir = 5;
                    }

                    Data Data = new Data();
                    Data.setR(String.valueOf(red1));
                    Data.setG(String.valueOf(green1));
                    Data.setB(String.valueOf(blue1));
                    Data.setKlasifikasi(String.valueOf(hasilakhir));
                    dbhelper.adddata(Data);

                    if(Data!=null){
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(InputData.this);
                        builder.setTitle("Save Data");
                        builder.setMessage("Data Berhasil disimpan");
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // TODO Auto-generated method stub
                                        Intent i = new Intent(InputData.this, MainActivity.class);
                                        startActivity(i);
                                    }
                                });
                        builder.create().show();
                    }


    }
});
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

 /*   @Override
    // method kamera kecil
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(InputData.this, InputData.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    // method permissions external storage kamera dan galery
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Ambil Foto"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Pilih dari Album"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    // method gambar kecil setelah identifikasi (pilihan)
    private void selectImage() {
        final CharSequence[] items = { "Ambil Foto", "Pilih dari Album", "Batal" };

        AlertDialog.Builder builder = new AlertDialog.Builder(InputData.this);
        builder.setTitle("Pilih Foto !!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(InputData.this);

                if (items[item].equals("Ambil Foto")) {
                    userChoosenTask ="Ambil Foto";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Pilih dari Album")) {
                    userChoosenTask ="Pilih dari Album";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Batal")) {
                    Intent i = new Intent(InputData.this, MainActivity.class);
                    startActivity(i);
                    dialog.dismiss();
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    // method galery
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    // method kamera
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    // method hasil kamera
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("Data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ivPickedImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    // method hasil galery
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        ivPickedImage.setImageBitmap(bm);
    }
}
