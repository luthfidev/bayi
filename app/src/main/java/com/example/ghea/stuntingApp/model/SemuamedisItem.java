package com.example.ghea.stuntingApp.model;

import com.google.gson.annotations.SerializedName;

public class SemuamedisItem {

    @SerializedName("no_medisanak")
    private String no_medisanak;

    public void setNo_medis(String no_medis){
        this.no_medisanak = no_medisanak;
    }

    public String getNo_medis(){
        return no_medisanak;
    }
    @Override
    public String toString(){
        return
                "SemuamedisItem{" +
                        "no_medisanak = '" + no_medisanak + '\'' +"}";
    }
}
