package com.example.ghea.stuntingApp.model;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseMedis {

    @SerializedName("semuamedis")
    private List<SemuamedisItem> semuamedis;

    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    public void setSemuamedis(List<SemuamedisItem> semuamedis){
        this.semuamedis = semuamedis;
    }

    public List<SemuamedisItem> getSemuamedis(){
        return semuamedis;
    }

    public void setError(boolean error){
        this.error = error;
    }

    public boolean isError(){
        return error;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    @Override
    public String toString(){
        return
                "ResponseMedis{" +
                        "semuamedis = '" + semuamedis + '\'' +
                        ",error = '" + error + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}