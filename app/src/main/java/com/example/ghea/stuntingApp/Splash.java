package com.example.ghea.stuntingApp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.ProgressBar;

public class Splash extends AppCompatActivity {
    // Splash screen timer
    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    private int Value = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

        progressBar=(ProgressBar)findViewById(R.id.progressBar1); // initiate the progress bar

        progressBar.setProgress(0); //Set Progress Dimulai Dari O

        // Handler untuk Updating Data pada latar belakang
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                // Menampung semua Data yang ingin diproses oleh thread
//                persentase.setText(String.valueOf(Value)+"%");
                if(Value == progressBar.getMax()){
//                    Toast.makeText(getApplicationContext(), "Progress Completed", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Splash.this, LoginActivity.class));
                    finish();
                }
                Value++;
            }
        };

        // Thread untuk updating progress pada ProgressBar di Latar Belakang
        Thread thread = new Thread( new Runnable() {
            @Override
            public void run() {
                try{
                    for(int w=0; w<=progressBar.getMax(); w++){
                        progressBar.setProgress(w); // Memasukan Value pada ProgressBar
                        // Mengirim pesan dari handler, untuk diproses didalam thread
                        handler.sendMessage(handler.obtainMessage());
                        Thread.sleep(40); // Waktu Pending 40ms/0.1 detik
                    }
                }catch(InterruptedException ex){
                    ex.printStackTrace();
                }
            }
        });
        thread.start();
    }
}

