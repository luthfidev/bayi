package com.example.ghea.stuntingApp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

public class DBhelper extends SQLiteOpenHelper {

    Data data_db ;
    private static final String DATABASE_NAME = "Data.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "data";
    public static final String COLUMN_R = "R";
    public static final String COLUMN_G= "G";
    public static final String COLUMN_B = "B";
    public static final String COLUMN_KLASIFIKASI = "Klasifikasi";
    SQLiteDatabase mDatabase;


    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                     "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_R +" TEXT, " +
                    COLUMN_G + " TEXT, " +
                    COLUMN_B + " TEXT, " +
                    COLUMN_KLASIFIKASI + " TEXT " +
                    ")";


    public DBhelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL(TABLE_CREATE);
    }

     boolean adddata(Data datadb){
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            String insertSQL = "INSERT INTO  \n" + TABLE_NAME +"("+
                    COLUMN_R +","+
                    COLUMN_G+","+
                    COLUMN_B+","+
                    COLUMN_KLASIFIKASI+
                    ")VALUES \n" +
                    "(?, ?, ?, ?);";

            //using the same method execsql for inserting values
            //this time it has two parameters
            //first is the sql string and second is the parameters that is to be binded with the query
            //mDatabase.execSQL(insertSQL, new String[]{datadb.getNama(), datadb.getText(), toString(datadb.getGambar()), "0"});
            SQLiteStatement insertStmt      =   db.compileStatement(insertSQL);
        insertStmt.clearBindings();
        insertStmt.bindString(1, datadb.getR());
        insertStmt.bindString(2, datadb.getG());
        insertStmt.bindString(3, datadb.getB());
        insertStmt.bindString(4, datadb.getKlasifikasi());
        insertStmt.executeInsert();
         db.setTransactionSuccessful();
         db.endTransaction();
         return true;
     }

    public ArrayList<Data> getAlldata() {
        String query = "SELECT * FROM " + TABLE_NAME
                ;
        ArrayList<Data> listdata = new ArrayList<Data>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(query, null);
        if (c != null) {
            while (c.moveToNext()) {
                Data Data = new Data();
                Data.setR(c.getString(c.getColumnIndex(COLUMN_R)));
                Data.setG(c.getString(c.getColumnIndex(COLUMN_G)));
                Data.setB(c.getString(c.getColumnIndex(COLUMN_B)));
                Data.setKlasifikasi(c.getString(c.getColumnIndex(COLUMN_KLASIFIKASI)));

                listdata.add(Data);
            }
        }
        Log.e("cek Data", String.valueOf(listdata));
//        Log.e("cek Data", DatabaseUtils.dumpCurrentRowToString(c));
        return listdata;

    }
    public void Delete_Data(String text)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_NAME+" WHERE "+COLUMN_R+"='"+text+"'");
        db.close();
    }
}
