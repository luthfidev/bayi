package com.example.ghea.stuntingApp.api;

/*
import com.meridianid.farizdotid.mahasiswaapp.model.ResponseDosen;
import com.meridianid.farizdotid.mahasiswaapp.model.ResponseDosenDetail;
import com.meridianid.farizdotid.mahasiswaapp.model.ResponseMatkul;*/

//import com.example.ghea.stuntingApp.model.ResponseAnak;

import com.example.ghea.stuntingApp.model.ResponseMedis;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface BaseApiService {

    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/login.php
    @FormUrlEncoded
    @POST("api_login.php")
    Call<ResponseBody> loginRequest(@Field("username") String username,
                                    @Field("password") String password);

    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/register.php
    @FormUrlEncoded
    @POST("register.php")
    Call<ResponseBody> registerRequest(@Field("nama") String nama,
                                       @Field("email") String email,
                                       @Field("password") String password);

    @FormUrlEncoded
    @POST("api/api_insert_ortu.php")
    Call<ResponseBody> simportuRequest(@Field("nama_ayah") String namaAyah,
                                       @Field("nik_ayah") String nikAyah,
                                       @Field("nama_ibu") String namaIbu,
                                       @Field("nik_ibu") String nikIbu,
                                       @Field("notelp") String noTelp,
                                       @Field("alamat") String alamat);



    @GET("api/api_nomedis.php")
    Call<ResponseMedis> getSemuaMedis();

  /*  @GET("dosen/{namadosen}")
    Call<ResponseDosenDetail> getDetailDosen(@Path("namadosen") String namadosen);
    @GET("matkul")
    Call<ResponseMatkul> getSemuaMatkul();
    @FormUrlEncoded
    @POST("matkul")
    Call<ResponseBody> simpanMatkulRequest(@Field("nama_dosen") String namadosen,
                                           @Field("matkul") String namamatkul);
    @DELETE("matkul/{idmatkul}")
    Call<ResponseBody> deteleMatkul(@Path("idmatkul") String idmatkul);*/
}
